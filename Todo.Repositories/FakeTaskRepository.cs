﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Todo.Entities;


namespace Todo.Repository
{
    public class FakeTaskRepository : ITaskRepository
    {
        private static List<TaskEntity> _TaskList;
        
        static FakeTaskRepository()
        {
            _TaskList = new List<TaskEntity>();

                _TaskList.Add(new TaskEntity()
                {
                    Id = 1,
                    Name = " Rebuild project",
                    IsDone = false,
                    Priority = 4,
                    UserId = 1
                });

                _TaskList.Add(new TaskEntity()
                {
                    Id = 2,
                    Name = " Sort array ",
                    IsDone = false,
                    Priority = 2,
                    UserId = 2
                });
                
                _TaskList.Add(new TaskEntity()
                {
                    Id = 3,
                    Name = " Restart Windows ",
                    IsDone = true,
                    Priority = 1,
                    UserId = 1
                
                });

            _TaskList.Add(new TaskEntity()
            {
                Id = 4,
                Name = " Init all tasks ",
                IsDone = false,
                Priority = 2,
                UserId = 2
            });

            _TaskList.Add(new TaskEntity()
            {
                Id = 5,
                Name = " Change notebook monitor ",
                IsDone = true,
                Priority = 3,
                UserId = 1
            });
        }

        public List<TaskEntity> GetAll()
        {
            return _TaskList;
        }
                
        public void Add(string NewTaskInfo, int NewPriority, int NewUserId)
        {
            _TaskList.Add(new TaskEntity()
            {
                Id = ( _TaskList.Max( item => item.Id )) + 1,
                Name = NewTaskInfo,
                IsDone = false,
                Priority = NewPriority,
                UserId = NewUserId
            });
                 
        }

        public void Remove(int RemoveId)
        {
            _TaskList.RemoveAll(item => (item.Id == RemoveId)); 
        }

        public TaskEntity GetTaskById(int Id)
        {
            TaskEntity TaskToEdit = new TaskEntity();
            return TaskToEdit = _TaskList.Find( item => (item.Id == Id) );
          
        }

        public void Update(TaskEntity Task)
        {

            _TaskList.Find(item => (item.Id == Task.Id)).IsDone = Task.IsDone;
            _TaskList.Find(item => (item.Id == Task.Id)).Name = Task.Name;
            _TaskList.Find(item => (item.Id == Task.Id)).Priority = Task.Priority;


        }

    }
}
