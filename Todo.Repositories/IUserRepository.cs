﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;

namespace Todo.Repositories
{
    public interface IUserRepository
    {
        UserEntity GetUserByName(string name);
        UserEntity GetUserById(int id);

    }
}
