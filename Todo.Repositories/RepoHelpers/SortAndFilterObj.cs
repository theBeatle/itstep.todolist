﻿namespace Todo.Repositories.RepoHelpers
{
    public class SortAndFilterObj
    {
        public int SortingColumn { set; get; }
        public int SortDirection { set; get; }
        public int FilteringColumn { set; get; }
        public int FilteringCriteria { set; get; }
    }
}
