﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;

namespace Todo.Repositories
{
    public class FakeUserRepository : IUserRepository
    {
        private static List<UserEntity> _UserList;

        static FakeUserRepository()
        {
            _UserList = new List<UserEntity>();

            _UserList.Add(new UserEntity()
            {
                UserId = 0,
                UserName = "admin",
                FullUserName = "App Admin",
                HashPassword = "C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC"
            });
            _UserList.Add(new UserEntity()
            {
                UserId = 1,
                UserName = "oleg",
                FullUserName = "Oleg Knyaz",
                HashPassword = "814A71AB96FDA17A1E8F71969ADD6F0672B39473EEB45D01480A3CB1575FC5CC91071081F065B585939095094813B8FD6D040437F05CF0BA68FAE1FD3BC26945"
            });
            _UserList.Add(new UserEntity()
            {
                UserId = 2,
                UserName = "ivan",
                FullUserName = "Ivan Bogun",
                HashPassword = "77B7248CF92D061250B5D51F2E12B22CD6102D82E796A1DD1369F82C0A7BF734B87DB911C5E876A1738E902F91907A14C57E861F9295EB7822D26B6145EDB4FC"
            });

        }

        public UserEntity GetUserById(int id)
        {
            return _UserList.Find(item => (item.UserId == id));
        }

        public UserEntity GetUserByName(string name)
        {
            return _UserList.Find(item => (item.UserName == name));
        }

    }
}
