use Tasks
drop table TodoList
create table TodoList
(
	id int identity primary key,
	Name nvarchar(32),
	IsDone nvarchar(32)
)

insert into TodoList(Name, IsDone) values
 ( 'reset',	'true'	) 
,( 'brake',		'false'	) 
,( 'save', 		'true'	) 
,( 'cut',  		'false'	) 
,( 'john',  	'true'	) 

select * from TodoList







