﻿using System.Collections.Generic;
using Todo.Entities;

namespace Todo.Repository
{
    public interface ITaskRepository
    {
        
        List<TaskEntity> GetAll();
        void Add(string NewTaskInfo, int NewPriority, int NewUserId);
        void Remove(int RemoveId);
        TaskEntity GetTaskById(int Id);
        void Update(TaskEntity Task);

    }
}
