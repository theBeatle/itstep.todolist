﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Todo.Tools.SHA512pass
{
    class Program
    {
        static void Main(string[] args)
        {
            string [] passwords = { "авалілво", "blabla1", "blabla2" };

            using (var sha512 = new System.Security.Cryptography.SHA512Managed())
            {
                foreach (var pass in passwords)
                {
                    byte[] textData = Encoding.UTF8.GetBytes(pass);
                    byte[] hash = sha512.ComputeHash(textData);
                    Console.WriteLine(BitConverter.ToString(hash).Replace("-", String.Empty));
                }
                Console.WriteLine("that's all!");
            }
        }



    }
}

