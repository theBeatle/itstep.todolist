using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using Todo.Repository;
using Todo.WebUI.Code.Managers;
using Todo.Repositories;
using Todo.WebUI.Code.HashService;

namespace Todo.WebUI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            //change type of repo if is needed
            //sql string conectionString = "";
            //sql  container.RegisterType<ITaskRepository, FakeTaskRepository>(new InjectionConstructor(conectionString));

            container.RegisterType<ITaskRepository, FakeTaskRepository>();
            container.RegisterType<ISecurityManager, FormsSecurityManager>();
            container.RegisterType<IUserRepository, FakeUserRepository>();
            container.RegisterType<IHashService, HashService>();

            //container.RegisterType<ISecurityManager, FormsSecurityManager>();        
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            //e.g. container.RegisterType<ITestService, TestService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}