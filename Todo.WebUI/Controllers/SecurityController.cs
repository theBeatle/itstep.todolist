﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Todo.WebUI.Code.Managers;
using Todo.WebUI.Models;

namespace Todo.WebUI.Controllers
{
    public class SecurityController : Controller
    {
        ISecurityManager _securityManager;
        public SecurityController(ISecurityManager securityManager)
        {
            this._securityManager = securityManager;
        } 

        // GET: Login
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if(_securityManager.Login(model.Login, model.Password))
            {
                return RedirectToAction("Index", "Task");
            }
            else
            {
                ViewBag.ErrorMessage = @"Incorect user login or password";
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            // clear authentication cookie
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);

            HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", "");
            cookie2.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie2);

            //FormsAuthentication.RedirectToLoginPage();
            return RedirectToAction("Login", "Security");
        }

        public ActionResult GetUserName()
        {
            LoginModel model = new LoginModel();
            model.Login = HttpContext.User.Identity.Name;
            return PartialView("UserStatePartialView", model);
        }

    }
}