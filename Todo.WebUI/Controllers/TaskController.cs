﻿using System.Collections.Generic;
using System.Web.Mvc;
using Todo.Entities;
using Todo.Repositories;
using Todo.Repository;
using Todo.WebUI.Models;

namespace Todo.WebUI.Controllers
{
    
    [Authorize]
    public class TaskController : Controller
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IUserRepository _userRepository;

        public TaskController(ITaskRepository taskRepository, IUserRepository userRepository)
        {
            _taskRepository = taskRepository;
            _userRepository = userRepository;
        }
                   
        #region HttpGet
        [HttpGet]
        public ActionResult Index()
        {

            var taskList = _taskRepository.GetAll();
            ViewBag.TaskArr = taskList;
  
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {

            var editTask = _taskRepository.GetTaskById(id);
            var newTaskModel = new TaskModel
            {
                Id = editTask.Id,
                Name = editTask.Name,
                IsDone = editTask.IsDone,
                Priority = editTask.Priority,
                UserId = _userRepository.GetUserByName(HttpContext.User.Identity.Name).UserId
            };
            return View(newTaskModel);
        }

        [HttpGet]
        public ActionResult SortByStatus(string status)
        {
            switch (status)
            {
                case "completed tasks":
                    return PartialView("TasksPartialView", _taskRepository.GetAll().FindAll(item => (item.IsDone == true)));
                case "tasks in progress":
                    return PartialView("TasksPartialView", _taskRepository.GetAll().FindAll(item => (item.IsDone == false)));
                default:
                    return PartialView("TasksPartialView", _taskRepository.GetAll());
            }
        }

        [HttpGet]
        public ActionResult SortByPriority(int priority)
        {
            return PartialView("TasksPartialView", priority < 5 ? _taskRepository.GetAll().FindAll(item => (item.Priority == priority)) : _taskRepository.GetAll());
        }

        #endregion

        #region HttpPost
        [HttpPost]
        public ActionResult Index(string NewTask)
        {
            const int priority = 0;
            _taskRepository.Add(NewTask, priority, this._userRepository.GetUserByName(HttpContext.User.Identity.Name).UserId);
            ViewBag.TaskArr = _taskRepository.GetAll();
            return View();
        }

        [HttpPost]
        public ActionResult SetStatus(int taskId, bool isDone)
        {
            if (AutorizedUser(_taskRepository.GetTaskById(taskId).UserId))
            {
                var task = _taskRepository.GetTaskById(taskId);
                task.IsDone = isDone;
                _taskRepository.Update(task);
                ViewBag.ErrMsg = "Action OK";
                return Json("");
            }
            ViewBag.ErrMsg = "you don't have enough permissions";
            return Redirect("Index");

        }

        [HttpPost]
        public ActionResult SetPriority(int taskId, int priorityNew)
        {
            if (AutorizedUser(_taskRepository.GetTaskById(taskId).UserId))
            {
                var task = _taskRepository.GetTaskById(taskId);
                task.Priority = priorityNew;
                _taskRepository.Update(task);
                ViewBag.ErrMsg = "Action OK";
                return Json("");               
            }
            ViewBag.ErrMsg = "you don't have enough permissions";
            return Redirect("Index");
        
        }

        [HttpPost]
        public ActionResult DeleteTask(int taskId)
        {
            if ( AutorizedUser(_taskRepository.GetTaskById(taskId).UserId))
            {
                _taskRepository.Remove(taskId);
                ViewBag.ErrMsg = "Action OK";
            }
            ViewBag.ErrMsg = "you don't have enough permissions";
            return Redirect("Index");
        }

        [HttpPost]
        public ActionResult SaveTask(TaskModel model)
        {
            if (AutorizedUser(_taskRepository.GetTaskById(model.Id).UserId))
            {
                var task = _taskRepository.GetTaskById(model.Id);
                task.Name = model.Name;
                task.IsDone = model.IsDone;
                _taskRepository.Update(task);
                ViewBag.ErrMsg = "Action OK";
            }
            ViewBag.ErrMsg = "you don't have enough permissions";
            return Redirect("Index");
        }
        #endregion

        #region Helpers
        bool AutorizedUser(int userId)
        {
            return HttpContext.User.Identity.Name.Equals(_userRepository.GetUserById(userId).UserName);
        }
        #endregion

    }
}
