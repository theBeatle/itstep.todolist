﻿namespace Todo.WebUI.Models
{
    public class TaskModel
    {
            public int Id { set; get; }
            public string Name { set; get; }
            public bool IsDone { set; get; }
            public int Priority { set; get; }
            public int UserId { set; get; }
    }
    
 }

