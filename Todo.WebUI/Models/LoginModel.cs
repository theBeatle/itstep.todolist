﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Todo.WebUI.Models
    {
        public class LoginModel
        {
            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "User name")]
            public string Login { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }
        }

    }
