﻿
(function () {
    var onCheckBoxClick = function (e) {
        var $el = $(e.target);
        var taskIdToSet = $el.data("id");
        var data = "";
        if ($el.is(":checked")) {
            $el.parent().parent().addClass("success");
            $el.parent().parent().removeClass("warning");
            data = {
                taskId: taskIdToSet,
                isDone: true
          
            };
        } else {
            $el.parent().parent().addClass("warning");
            $el.parent().parent().removeClass("success");
            data = {
                taskId: taskIdToSet,
                isDone: false
            };
        }
        //AJAX function
        $.ajax({
            url: "/Task/SetStatus",
            method: "POST",
            dataType: "json",
            data
        }).done(function () {
            if ($el.is(":checked") === true) {
                $el.parent().parent().addClass("isDone");
            }
            console.log("CheckBox taskID=" + data.taskId + " AJAX done and Task is set to:" + data.isDone);
        }).fail(function () {
            console.log("CheckBox taskID=" + data.taskId + " AJAX failed");

            if ($el.is(":checked") === true) {
                $el.removeAttr("checked");
            } else {
                $el.attr("checked");
            }
        });
    };

    var onSortSelectChange = function (e) {
        var $el = $(e.target);
        $.ajax({
            url: "/Task/SortByStatus",
            method: "GET",
            dataType: "html",
            data: "status="+$el.val() 
        }).done(function (data) {
            $("tbody").html(data);
            init();
            console.log("Select SortByStatus AJAX done with '" + $el.val() + "' value");

        }).fail(function () {
            console.log("Select SortByStatus AJAX FAILED with '" + $el.val() + "' value");
        });
    };

    var onPrioritySortSelectChange = function (e) {
        var $el = $(e.target);
        $.ajax({
            url: "/Task/SortByPriority",
            method: "GET",
            dataType: "html",
            data: "priority=" + $el.val()
        }).done(function (data) {
            $("tbody").html(data);
            console.log("Select SortByPriority AJAX done with '" + $el.val() + "' value");
            init();
        }).fail(function () {
            console.log("Select SortByPriority AJAX FAILED with '" + $el.val() + "' value");
        });
    };

    var onSetPrioritySelectChange = function (e) {
        var $el = $(e.target);
        var taskIdToSet = $el.data("id");
        var data = "";
        var styles = new Array("btn-default", "btn-info", "btn-success", "btn-warning", "btn-danger");
        data = {
            taskId: taskIdToSet,
            priorityNew: $el.val()
        };
        console.log(data.taskId+ " " + data.priorityNew);
        
        //AJAX function
        $.ajax({
            url: "/Task/SetPriority",
            method: "POST",
            dataType: "json",
            data
        }).done(function () {
            $el.removeClass(function (index, classes) {
                var matches = classes.match(/\bbtn-\S+/ig);
                return (matches) ? matches.join(" ") : "";
            }); //clears all 'btn-' styles to add new one
            $el.addClass(styles[data.priorityNew]);
            console.log("SetPriority changed AJAX done");
        }).fail(function () {
            console.log("SetPriority changed AJAX FAILED");
        }); 
    };

    var onSortTableOnColumn = function (e) {
        var el = e.target;
        var $el = $(e.target);
        var index = $el.closest("th").index();
        console.log("sort invoke column index = " + index);
        var rows = $("#mytable tbody tr").get();

        var f; //sort direction

        console.log("1st check "+el.classList);

        if ($el.hasClass('sort-alpha'))
        {
            f = 1;
            $el.removeClass("sort-alpha").addClass("sort-asc");
            console.log("2nd check " + el.classList);
        } else if ($el.hasClass("sort-asc")) {
            f = -1;
            $el.removeClass("sort-asc").addClass("sort-dsc");
            console.log("2nd check " + el.classList);
        } else if ($el.hasClass('sort-dsc')) {
            f = 1;
            $el.removeClass("sort-dsc").addClass("sort-asc");
            console.log("2nd check " + el.classList);
        }
        
        rows.sort(function (a, b) {
            var aA, bB;
            if (index === 0 || index === 1 || index === 5) {
                aA = $(a).children("td").eq(index).text().toUpperCase();
                bB = $(b).children("td").eq(index).text().toUpperCase();
            } else if (index === 2) {
                aA = $(a).children("td").eq(index).children().is(':checked');
                bB = $(b).children("td").eq(index).children().is(':checked');
            } else if (index === 3) { 
                //if Priority sorting 
                aA = $(a).children("td").eq(index).children().val();
                bB = $(b).children("td").eq(index).children().val();
            } else {
                return 0;
            }
            return aA < bB ? -1*f : aA > bB ? 1*f : 0;
            });
        $.each(rows, function (index, row) {
            $("#mytable").find("tbody").append(row);
            });
       
    };

    var onHoverTableHeader = function (e) {
        var $el = $(e.target);
        $el.css("cursor", "pointer");
        $el.addClass("active");
    };

    var onHoverOutTableHeader = function (e) {
        var $el = $(e.target);
        $el.css("cursor", "default");
        $el.removeClass("active");
    };

    var init = function () {
        $(".is-done-checkbox").on("click", onCheckBoxClick);
        $(".sort-by-status").on("change", onSortSelectChange);
        $(".sort-by-priority").on("change", onPrioritySortSelectChange);
        $(".select-set-priority").on("change", onSetPrioritySelectChange);
        $(".sortable").on("click", onSortTableOnColumn);
        $(".sortable").on("mouseover", onHoverTableHeader);
        $(".sortable").on("mouseout", onHoverOutTableHeader);
     };

    $(function () {
        init();
        console.log("Init is done. Form loaded"); //"main" function
    });
})();
