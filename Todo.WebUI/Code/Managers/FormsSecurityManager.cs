﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Todo.Repositories;
using Todo.WebUI.Code.HashService;

namespace Todo.WebUI.Code.Managers
{
    public class FormsSecurityManager : ISecurityManager
    {
        private IUserRepository _userRepository;
        private IHashService _hashService;

        public FormsSecurityManager(IUserRepository userRepository, IHashService hashService)
        {
            this._userRepository = userRepository;
            this._hashService = hashService;
        }

        public bool Login(string login, string password)
        {
            var user = _userRepository.GetUserByName(login);
            if (user != null)
            {
                var tempPass = this._hashService.Hash(password);
                if (string.Equals(user.HashPassword, tempPass, StringComparison.Ordinal))
                {
                    FormsAuthentication.SetAuthCookie(login, false);
                    return true;
                }
            }   
            return false;
          
        }
        public bool IsAuthenticated()
        {
            var user = HttpContext.Current.User;
            return ((user != null)&&(user.Identity != null)&&(user.Identity.IsAuthenticated == true));
       }

        public void Logout()
        {
            FormsAuthentication.SignOut();
      
        }
    }
}