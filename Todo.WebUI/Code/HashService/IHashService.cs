﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.WebUI.Code.HashService
{
    public interface IHashService
    {
        string Hash(string tmpPassword);
    }
}
