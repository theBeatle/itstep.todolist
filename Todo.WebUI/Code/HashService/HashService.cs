﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Todo.WebUI.Code.HashService
{
    public class HashService : IHashService
    {
        public string Hash(string password)
        {
            if (String.IsNullOrEmpty(password))
            {
                return String.Empty;
            }

            using (var sha512 = new System.Security.Cryptography.SHA512Managed())
            {
                byte[] textData = Encoding.UTF8.GetBytes(password);
                byte[] hash = sha512.ComputeHash(textData);
                string HashToReturn = BitConverter.ToString(hash).Replace("-", String.Empty);
                return HashToReturn;
            }
        }
    }
}